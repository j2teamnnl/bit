<?php

Route::get('test','Controller@test');

Route::group(['middleware' => 'CheckLogin'],function(){
	Route::group(['prefix' => 'lop', 'as' => 'lop.'],function(){
		Route::get('','LopController@view_all')->name('view_all');
		Route::get('view_insert','LopController@view_insert')->name('view_insert');
		Route::post('process_insert','LopController@process_insert')->name('process_insert');


		Route::get('view_update/{ma}','LopController@view_update')->name('view_update');
		Route::post('process_update/{ma}','LopController@process_update')->name('process_update');
		Route::get('delete/{ma}','LopController@delete')->name('delete');
	});


	Route::group(['prefix' => 'sinh_vien', 'as' => 'sinh_vien.'],function(){
		Route::get('','SinhVienController@view_all')->name('view_all');
		Route::get('view_insert','SinhVienController@view_insert')->name('view_insert');
		Route::post('process_insert','SinhVienController@process_insert')->name('process_insert');


		Route::get('view_update/{ma}','SinhVienController@view_update')->name('view_update');
		Route::post('process_update/{ma}','SinhVienController@process_update')->name('process_update');
		Route::get('delete/{ma}','SinhVienController@delete')->name('delete');
	});
});

Route::get('login','Controller@login')->name('view_login');
Route::post('process_login','Controller@process_login')->name('process_login');

Route::get('logout','Controller@logout')->name('logout');