<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="../assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Light Bootstrap Dashboard PRO by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="{{ asset('css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>



    <!--     Fonts and icons     -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href='{{ asset('css/font.css') }}' rel='stylesheet' type='text/css'>

</head>
<body>

<div class="wrapper">
	@include('layer.menu')
        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        

    <div class="main-panel">
        @include('layer.header')


        <div class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">

                        <div class="card card-calendar">
                            <div class="content">
                               @yield('body')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       @include('layer.footer')


    </div>
</div>


</body>
    <!--   Core JS Files  -->
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/light-bootstrap-dashboard.js') }}" type="text/javascript"></script>


</html>
