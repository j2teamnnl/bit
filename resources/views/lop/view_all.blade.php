@extends('layer.master')

@section('body')
<a href="{{ route('lop.view_insert') }}">
	Thêm
</a>
<table class="table">
	<tr>
		<th>Mã</th>
		<th>Tên</th>
		<th>Sửa</th>
		<th>Xoá</th>
	</tr>
	@foreach ($array as $each)
		<tr>
			<td>{{$each->ma}}</td>
			<td>{{$each->ten}}</td>
			<td>
				<a href="{{ route('lop.view_update',['ma' => $each->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('lop.delete',['ma' => $each->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>
@endsection