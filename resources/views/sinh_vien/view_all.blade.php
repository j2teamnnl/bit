<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<a href="{{ route('sinh_vien.view_insert') }}">
	Thêm
</a>
<table border="1" width="100%">
	<tr>
		<th>Mã</th>
		<th>Tên</th>
		<th>Tuổi</th>
		<th>Tên Lớp</th>
		<th>Sửa</th>
		<th>Xoá</th>
	</tr>
	@foreach ($array as $each)
		<tr>
			<td>{{$each->ma}}</td>
			<td>{{$each->ten}}</td>
			<td>{{$each->tuoi}}</td>
			<td>{{$each->ten_lop}}</td>
			<td>
				<a href="{{ route('sinh_vien.view_update',['ma' => $each->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('sinh_vien.delete',['ma' => $each->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>
</body>
</html>