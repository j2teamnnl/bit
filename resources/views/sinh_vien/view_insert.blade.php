<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<form action="{{ route('sinh_vien.process_insert') }}" method="post">
	{{csrf_field()}}
	Tên
	<input type="text" name="ten">
	<br>
	Tuổi
	<input type="text" name="tuoi">
	<br>
	Lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{$lop->ma}}">
				{{$lop->ten}}
			</option>
		@endforeach
	</select>
	<br>
	<button>Thêm</button>
</form>
</body>
</html>