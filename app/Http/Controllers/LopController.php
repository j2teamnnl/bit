<?php 

namespace App\Http\Controllers;

use App\Http\Model\LopModel;
use Illuminate\Http\Request;

class LopController
{
	function view_all(){
		$array = LopModel::get_all();

		return view('lop.view_all',compact('array'));
	}
	function view_insert(){
		return view('lop.view_insert');
	}
	function process_insert(Request $rq){
		$lop = new LopModel();
		$lop->ten = $rq->get('ten');
		$lop->insert();

		// return redirect()->route("lop.view_all");
	}
	function view_update($ma)
	{
		$each = LopModel::get_one($ma);

		return view('lop.view_update',compact('each'));
	}
	function process_update($ma, Request $rq)
	{
		$lop = new LopModel();
		$lop->ma = $ma;
		$lop->ten = $rq->get('ten');
		$lop->update();

		// return redirect()->route("lop.view_all");
	}
	function delete($ma){
		LopModel::delete($ma);

		// return redirect()->route("lop.view_all");
	}
}