<?php 

namespace App\Http\Controllers;

use App\Http\Model\LopModel;
use App\Http\Model\SinhVienModel;
use Illuminate\Http\Request;

class SinhVienController
{
	function view_all(){
		$array = SinhVienModel::get_all();

		return view('sinh_vien.view_all',compact('array'));
	}
	function view_insert(){
		$array_lop = LopModel::get_all();

		return view('sinh_vien.view_insert',compact('array_lop'));
	}
	function process_insert(Request $rq){
		$sinh_vien = new SinhVienModel();
		$sinh_vien->ten = $rq->get('ten');
		$sinh_vien->tuoi = $rq->get('tuoi');
		$sinh_vien->ma_lop = $rq->get('ma_lop');
		$sinh_vien->insert();

		// return redirect()->route("sinh_vien.view_all");
	}
	function view_update($ma)
	{
		$each = SinhVienModel::get_one($ma);
		$array_lop = LopModel::get_all();

		return view('sinh_vien.view_update',compact('each','array_lop'));
	}
	function process_update($ma, Request $rq)
	{
		$sinh_vien = new SinhVienModel();
		$sinh_vien->ma = $ma;
		$sinh_vien->ten = $rq->get('ten');
		$sinh_vien->tuoi = $rq->get('tuoi');
		$sinh_vien->ma_lop = $rq->get('ma_lop');
		$sinh_vien->update();

		return redirect()->route("sinh_vien.view_all");
	}
	function delete($ma){
		SinhVienModel::delete($ma);

		// return redirect()->route("sinh_vien.view_all");
	}
}