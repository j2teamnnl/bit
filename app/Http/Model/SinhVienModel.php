<?php 

namespace App\Http\Model;

use DB;

class SinhVienModel
{
	public $ma;
	public $ten;
	public $tuoi;
	public $ma_lop;
	public $ten_lop;
	static function get_all(){
		$array = DB::select('select sinh_vien.*,lop.ten as ten_lop from sinh_vien
			join lop on lop.ma = sinh_vien.ma_lop
			');
		return $array;
	}
	public function insert(){
		DB::insert("insert into sinh_vien(ten,tuoi,ma_lop) values (?,?,?)",[
			$this->ten,
			$this->tuoi,
			$this->ma_lop
		]);
	}
	static function get_one($ma){
		$array = DB::select('select * from sinh_vien where ma = ?',[
			$ma
		]);
		return $array[0];
	}
	public function update()
	{
		DB::update("update sinh_vien set 
			ten = ?,
			tuoi = ?,
			ma_lop = ?
			where ma = ?",[
			$this->ten,
			$this->tuoi,
			$this->ma_lop,
			$this->ma
		]);
	}
	static function delete($ma){
		DB::delete("delete from sinh_vien where ma = ?",[
			$ma
		]);
	}
}