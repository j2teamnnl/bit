<?php 

namespace App\Http\Model;

use DB;

class LopModel
{
	public $ma;
	public $ten;
	static function get_all(){
		$array = DB::select('select * from lop');
		return $array;
	}
	public function insert(){
		DB::insert("insert into lop(ten) values (?)",[
			$this->ten
		]);
	}
	static function get_one($ma){
		$array = DB::select('select * from lop where ma = ?',[
			$ma
		]);
		return $array[0];
	}
	public function update()
	{
		DB::update("update lop set ten = ? where ma = ?",[
			$this->ten,
			$this->ma
		]);
	}
	static function delete($ma){
		DB::delete("delete from lop where ma = ?",[
			$ma
		]);
	}
}